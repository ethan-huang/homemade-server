// File to server
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <errno.h>
#include "util.h"
#define MAX_EVENTS 1024
#define BUFFER_SIZE 1024
void setnonblocking(int fd)
{
    fcntl(fd, F_SETFL, fcntl(fd, F_GETFL) | O_NONBLOCK);
}

int main()
{
    // 创建一个socket地址的struct
    struct sockaddr_in serv_addr;
    // 创建一个socket
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    errif(sockfd == -1, "socket create error in server");
    bzero(&serv_addr, sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    serv_addr.sin_port = htons(8888);
    // 首先得把服务器的ip和port number定好，然后和socket绑定一块：bind
    // 然后监听这个ip和port number的socket
    int bind_res = bind(sockfd, (sockaddr *)&serv_addr, sizeof(serv_addr));
    errif(bind_res == -1, "binding error in server");
    int list_res = listen(sockfd, SOMAXCONN);
    errif(list_res == -1, "binding error in server");

    // 开始使用epoll
    int epfd = epoll_create1(0);
    errif(epfd == -1, "epoll create error in server");
    struct epoll_event events[MAX_EVENTS], ev;
    bzero(&events, sizeof(events));
    bzero(&ev, sizeof(ev));
    ev.events = EPOLLIN;
    ev.data.fd = sockfd;
    setnonblocking(sockfd);
    epoll_ctl(epfd, EPOLL_CTL_ADD, sockfd, &ev);
    while (true)
    {
        int nfds = epoll_wait(epfd, events, MAX_EVENTS, -1);
        errif(nfds == -1, "epoll wait error in server");
        for (int i = 0; i < nfds; i++)
        { // 发生事件的fd是服务器socket fd，表示有新的客户端发起连接
            if (events[i].data.fd == sockfd)
            {
                struct sockaddr_in clnt_addr;
                bzero(&clnt_addr, sizeof(clnt_addr));
                socklen_t clnt_addr_len = sizeof(clnt_addr);
                int clnt_sockfd = accept(sockfd, (sockaddr *)&clnt_addr, &clnt_addr_len);
                errif(clnt_sockfd == -1, "accept error in server");
                printf("new client fd %d, IP: %s Port: %d\n", clnt_sockfd, inet_ntoa(clnt_addr.sin_addr), ntohs(clnt_addr.sin_port));

                bzero(&ev, sizeof(ev));
                ev.data.fd = clnt_sockfd;
                ev.events = EPOLLIN | EPOLLET;
                setnonblocking(clnt_sockfd);
                epoll_ctl(epfd, EPOLL_CTL_ADD, clnt_sockfd, &ev);
            }
            // 发生事件的是客户端，并且是可读事件（EPOLLIN）
            else if (events[i].events && EPOLLIN)
            {
                char buf[BUFFER_SIZE];
                // 因为使用非阻塞的io，即edge triggered，只会提醒一次，那一次性就得处理完所有数据
                while (true)
                {
                    bzero(&buf, sizeof(buf));
                    // 从文件描述符 events[i].data.fd 读取数据并存储到缓冲区 buf 中，最多读取 sizeof(buf) 字节的数据
                    ssize_t bytes_read = read(events[i].data.fd, buf, sizeof(buf));
                    if (bytes_read > 0)
                    {
                        printf("message from client fd %d: %s\n", events[i].data.fd, buf);
                        write(events[i].data.fd, buf, sizeof(buf));
                    }
                    else if (bytes_read == -1 && errno == EINTR)
                    {
                        printf("client quit normally, and continue reading data from clients");
                    }
                    else if (bytes_read == -1 && (errno == EAGAIN || errno == EWOULDBLOCK))
                    {
                        // 数据读取完毕,直接break退出
                        printf("finish reading once, errno: %d\n", errno);
                        break;
                    }
                    else if (bytes_read == 0)
                    {
                        // EOF, 客户端可以断开连接了
                        printf("EOF, client fd %d disconnected\n", events[i].data.fd);
                        close(events[i].data.fd); // 关闭socket会自动将文件描述符从epoll树上移除
                        break;
                    }
                }
            }
            else
            {
                printf("something else to do\n");
            }
        }
    }
    close(sockfd);
    return 0;
}
